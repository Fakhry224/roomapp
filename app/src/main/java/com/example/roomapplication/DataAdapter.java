package com.example.roomapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder> {
    List<Item> items;

    DataAdapter(){
        this.items = new ArrayList<>();
    }

    public void setItems(List<Item> items){
        this.items = items;
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        TextView tvID;
        TextView tvJudul;

        DataViewHolder(@NonNull View itemView){
            super(itemView);
            tvID = itemView.findViewById(R.id.idText);
            tvJudul = itemView.findViewById(R.id.judulText);
        }
    }

    @Override
    public DataViewHolder onCreateViewHolder(
            @NonNull ViewGroup parent, int viewType
    ) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_item, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(
            @NonNull DataViewHolder holder, int position
    ) {
        Item item = items.get(position);
        holder.tvID.setText(String.valueOf(item.getId()));
        holder.tvJudul.setText(item.getJudul());
    }

    @Override
    public int getItemCount(){
        return items.size();
    }
}
