package com.example.roomapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    Toolbar toolbar;
    DataAdapter adapter;
    Button bt1, bt2;
    EditText et1;

    private AppDatabase appDb;
//    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("225150401111005");

        appDb = AppDatabase.getInstance(getApplicationContext());

        et1 = findViewById(R.id.et1);
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);

        rv1 = findViewById(R.id.recycker_view);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        adapter = new DataAdapter();
        rv1.setAdapter(adapter);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item item = new Item();
                item.setJudul(et1.getText().toString());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        appDb.itemDao().insertAll(item);
                    }
                });
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        List<Item> list = appDb.itemDao().getAll();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.setItems(list);
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
            }
        });
    }
}


